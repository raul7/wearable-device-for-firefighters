This is the code for my project "Wireless Device for Firefighters" that I submitted for the "Challenge Climate Change with QuickLogic Corp. and SensiML" contest.

Despite the availability of wearable devices for some safety and security related applications in diverse areas, apparently there's not much of such devices for improving safety for firefighters. For instance, monitoring position and vital signs of firefighters could be of critical importance when they get isolated from their crew mates in dangerous situations. It becomes worst if they lose consciousness without no other crew mate near to notice.

The Wearable Device for Forest Firefighters (WDFF) is a wristband wearable device capable of monitoring vital signs, position, motion and gestures of the firefighters in real-time. The system performs the following tasks:

- It monitors the firefighter's geolocation (latitude and longitude) and vital signs, such as heart rate, Saturation of peripheral Oxygen (SpO2), body temperature and respiratory rate.
- It monitors ergonomics and motion to recognize the following activities and events: walking, running, using a chain saw, using an ax, an S.O.S. gesture (for when the firefighter is unable to talk) and idle (such as when a firefighter loses consciousness).
- It also monitors the following environment variables: Level of carbon dioxide (CO2), total volume of organic compounds (TVOC), barometric pressure, ambient temperature and relative humidity.

This project is in a 'proof-of-concept' stage. Some parts of the code need to be reworked, but is functional.

Note: There are a couple of files in the Android app project that are not needed, but they are there for future tests. If you have experience with Android development, you'll notice them easily. Plus, some code comments are in Spanish.