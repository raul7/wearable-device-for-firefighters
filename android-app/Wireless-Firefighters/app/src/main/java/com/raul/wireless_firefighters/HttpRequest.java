package com.raul.wireless_firefighters;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

public class HttpRequest extends AsyncTask<String,Void,String> {
    Context contexto;

    HttpRequest(Context ctx) {
        contexto = ctx;
    }

    ProgressDialog dialog;

    @Override
    protected String doInBackground(String... params) {
        // Direccion del script PHP que atiende requerimiento de lectura de base de datos
//        String url_servidor = "http://tec.bo/pablo/leer_android.php"; // Direccion absoluta
        String url_servidor = MainActivity.dirServidorWeb + "/leer_android.php"; // Direccion relativa

        // Valor del par (clave, valor) para envio mediante POST (no se usa por ahora)
//        String tipo = "httpRequestTest";
        String tipo = params[0];

        try {
            URL url = new URL(url_servidor);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setDoInput(true);
            OutputStream outputStream = httpURLConnection.getOutputStream();
            BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
            String post_data = URLEncoder.encode("tipo", "UTF-8") + "=" + URLEncoder.encode(tipo, "UTF-8");
            bufferedWriter.write(post_data);
            bufferedWriter.flush();
            bufferedWriter.close();
            outputStream.close();
            InputStream inputStream = httpURLConnection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
            String resultado = "";
            String linea = "";

            // Objeto StringBuilder para leer la cadena desde el servicio
            StringBuilder sb = new StringBuilder();

            while ((linea = bufferedReader.readLine()) != null) {
                resultado += linea;
                // Anadirlo al string builder
                sb.append(linea + "\n");
            }
            bufferedReader.close();
            inputStream.close();
            httpURLConnection.disconnect();
//            return resultado;
            return sb.toString().trim();

        } catch (MalformedURLException e) {
            Log.d("HTTP", "Error MalformedURLException");
            e.printStackTrace();
        } catch (IOException e) {
            Log.d("HTTP", "IOException");
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        // Para mostrar un dialogo de progreso
        dialog = new ProgressDialog(contexto);
        dialog.setMessage("Actualizando datos locales...");
        dialog.setTitle("CONEXION AL SERVIDOR");
        dialog.setIcon(R.mipmap.ic_launcher);
        dialog.show();
        dialog.setCancelable(false);
    }

    @Override
    protected void onPostExecute(String resultado) {
        Log.e("RESULTADO HTTP", " >>> " + resultado);
        dialog.cancel();

        if(resultado != null) {
            // Desglosar datos JSON recibidos desde el servidor y actualizar datos locales
            // de IDs y audiios
            JSONArray jsonArray = null;
            try {
                jsonArray = new JSONArray(resultado);

                // Iterar sobre todos los elementos en el array JSON
                for (int i = 0; i < jsonArray.length(); i++) {
                    // Leer un objeto JSON del array JSON
                    JSONObject obj = jsonArray.getJSONObject(i);

                    // Leer nombres desde el objeto JSON y anadirlos a la tabla local
                    MainActivity.setListaAudio(obj.getString("idfaro"), obj.getString("diraudio"));
                    Log.e("JSON ", "idfaro: " + obj.getString("idfaro") + " | diraudio: " + obj.getString("diraudio"));

                }
            } catch (JSONException e) {
                Log.d("JSON", "Error desglose JSON");
                e.printStackTrace();
            }
        }
        else {
            Log.d("JSON", "resultado es null");
        }

//        // Solo para depuracion:
//        // Imprimir la nueva tabla descargada desde el servidor
//        Log.d("TABLA", "Tabla descargada desde servidor:");
//        int tamanoTabla = MainActivity.listaAudios.size();
//        for(int i = 0; i < tamanoTabla; ++i) {
//            String idActualTabla = MainActivity.listaAudios.get(i)[0]; // Leer las IDs de la tabla una por una
//            Log.d("TABLA", "idActualTabla: " + idActualTabla + " | Audio:" + MainActivity.listaAudios.get(i)[1]);
//        }

        String fileName = "tabla";
//        String path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS ).getAbsolutePath().toString();

        // Si la tabla de audios esta vacia por falta de conexion a Internet
        if(MainActivity.listaAudios.size() == 0 ) {
            Toast.makeText(contexto, "No hay conexión a Internet, leyendo tabla local", Toast.LENGTH_SHORT).show();
            String directorio = contexto.getFilesDir().toString();
            Log.d("TABLA", "directorio: " + directorio);
            try {
                FileInputStream fis = contexto.openFileInput(fileName);
                ObjectInputStream is = new ObjectInputStream(fis);
                MainActivity.listaAudios = (ArrayList<String[]>) is.readObject();
                is.close();
                fis.close();
            } catch (IOException e) {
                Log.d("TABLA", "IOException al leer de telefono");
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

            // Solo para depuracion:
            // Imprimir la nueva tabla descargada desde el servidor
            Log.d("TABLA", "Tabla leida desde telefono:");
            int tamanoTabla = MainActivity.listaAudios.size();
            for(int i = 0; i < tamanoTabla; ++i) {
                String idActualTabla = MainActivity.listaAudios.get(i)[0]; // Leer las IDs de la tabla una por una
                Log.d("TABLA", "idActualTabla: " + idActualTabla + " | Audio:" + MainActivity.listaAudios.get(i)[1]);
            }

        } else { // Se conecto a Internet, guardar nueva tabla
            Toast.makeText(contexto, "Tabla leida desde Internet exitosamente", Toast.LENGTH_SHORT).show();
            try {

                FileOutputStream fos = contexto.openFileOutput(fileName, Context.MODE_PRIVATE);
                ObjectOutputStream os = new ObjectOutputStream(fos);
                os.writeObject(MainActivity.listaAudios);
                os.close();
                fos.close();
                Log.d("TABLA", "Se escribio a telefono");
            } catch (IOException e) {
                Log.d("TABLA", "IOException al escribir a telefono");
                e.printStackTrace();
            }
        }


    }
}
