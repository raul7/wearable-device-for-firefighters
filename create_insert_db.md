# Database Details

After creating your database, configure the database name,user name and password in the 'login.php' file:

<?php // login.php
  $hn = 'localhost';
  $db = '<put-your-db-name-here>';
  $un = '<put-your-user-name-here>';
  $pw = '<put-your-password-here>';
?>

# Create the table

To create the table, in you PhpMyAdmin console run:

CREATE TABLE wff_readings (
    unix_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    name VARCHAR(255) NOT NULL,
    id_code CHAR(38) NOT NULL,
    latitude FLOAT(11,7) NULL ,
    longitude FLOAT(11,7) NULL ,
    status CHAR(32) NOT NULL,
    sensors VARCHAR(255) NOT NULL,
    id INT UNSIGNED NOT NULL AUTO_INCREMENT KEY
) ENGINE MyISAM;


# Insert Into the Table:

INSERT INTO wff_readings (unix_time, name, id_code, latitude, longitude, status, sensors)
 VALUES('2021-05-12 15:21:53', 'Raul Alvarez', 'RA12346789', '-17.3970885', '-66.1538688', 'normal', '{"class":2,"body_temp":37.56,"heart_rate":75.94,"spo2":98.03,"amb_temp":20.68,"rel_hum":33.36,"bar_pressp":748.5,"eco2":545,"etvoc":22}');


# Insert Remotely by Sending a cURL HTTP POST request to 'receive_readings.php':

**Escaped double quotes to avoid getting them deleted:**

curl --data "unix_time=1622062265&name=Raul Alvarez&id_code=RA12346789&latitude=-17.419655&longitude=-66.1645471&status=normal&sensors={\"class\":2,\"body_temp\":37.56,\"heart_rate\":76.38,\"spo2\":98.32,\"amb_temp\":20.68,\"rel_hum\":33.36,\"bar_pressp\":74.85,\"eco2\":545,\"etvoc\":22}" <put-your-server-url-here>/readings/receive_readings.php


# To Delete All Rows in a Table

TRUNCATE TABLE wff_readings
