#ifndef __MODEL_JSON_H__
#define __MODEL_JSON_H__

const char recognition_model_string_json[] = {"{\"NumModels\":1,\"ModelIndexes\":{\"0\":\"MyPipeline_rank_2\"},\"ModelDescriptions\":[{\"Name\":\"MyPipeline_rank_2\",\"ClassMaps\":{\"1\":\"exec_mission_drone\",\"2\":\"idle\",\"3\":\"land_drone\",\"4\":\"patrol_drone\",\"5\":\"rtl_drone\",\"6\":\"running\",\"7\":\"sos\",\"8\":\"takeoff_drone\",\"9\":\"using_ax\",\"10\":\"using_chainsaw\",\"11\":\"walking\",\"0\":\"Unknown\"},\"ModelType\":\"PME\"}]}"};

int recognition_model_string_json_len = sizeof(recognition_model_string_json);

#endif /* __MODEL_JSON_H__ */