/** @file sensor_ssss_process.c */

/*==========================================================
 * Copyright 2020 QuickLogic Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *==========================================================*/

#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "timers.h"

#include "datablk_mgr.h"
#include "process_ids.h"
#include "datablk_processor.h"
#include "sensor_ssss.h"
#include "micro_tick64.h"
#include "dbg_uart.h"
#include "ssi_comms.h"
#include "eoss3_hal_i2c.h"

#include "mc3635_wire.h"
#include "sml_recognition_run.h"
#include "DataCollection.h"

// When enabled, GPIO (configured in pincfg_table.c) is toggled whenever a
// datablock is dispacthed for writing to the UART. Datablocks are dispatched
// every (SENSOR_SSSS_LATENCY) ms
#if (SENSOR_SSSS_RATE_DEBUG_GPIO == 1)
#include "eoss3_hal_gpio.h"
uint8_t sensor_rate_debug_gpio_val = 1;
#endif

/* BEGIN user include files
 * Add header files needed for accessing the sensor APIs
 */
#include "BARO/BARO.h" // For LPS22HB sensor
//#include "LPS22HBSensor.h" //
#include "Sparkfun/SparkFunCCS811.h"
#include "DFRobot_CCS811/DFRobot_CCS811.h"
#include "MCP9800jc/MCP9800.h"
//#include "MAX30100lib/MAX30100_PulseOximeter.h" // For 'oxullo' version
#include "MAX30100ktk/MAX30100.h" // For the 'kontakt' library version
#include "AHT10/AHT10.h"

extern "C" void HAL_DelayUSec(uint32_t usecs);
extern void delay(unsigned long ms);
extern unsigned long millis();

/* Variables for sensor readings */
#define CCS811_ADDR 0x5A // 0x5B is an Alternate I2C Address
uint16_t ccsEco2;
uint16_t ccsEtvoc;
uint16_t lpsPressure;
int16_t lpsTemperature;
int16_t mcpTemperature;
uint16_t mxmHeartRate;
uint8_t mxmSpO2;
int16_t ahtTemperature;
uint16_t ahtRHumidity;
char dbgString1[50]; // Buffer for debugging-strings

/* Variables for sending data to mobile device */
int16_t bodyTemperature;
uint16_t heartRate;
uint8_t spO2;
int16_t ambTemperature;
uint16_t relHumidity;
int16_t barPressure;
uint16_t eCo2;
uint16_t eTvoc;

/* User settable MACROs */

/* This section defines MACROs that may be user modified */

MC3635  qorc_ssi_accel;
/* >>>>>>>>>>>>>>>>>>>> ForLPS22HB */
//LPS22HBClass BARO(Wire); // Object already created in library's BARO.cpp!
CCS811 SF_CCS811(CCS811_ADDR); // Sparkfun (works!)
//DFRobot_CCS811 CCS811; // DFRobot (works!)
//DFRobot_CCS811 CCS811(&Wire, /*IIC_ADDRESS=*/0x5A); // DFRobot (works!)
MCP9800 MCP9800; // MCP9800 sensor
//Weather sensor; // sensor
//PulseOximeter pox;
AHT10 myAHT10(AHT10_ADDRESS_0X38);

// Callback (registered below) fired when a pulse is detected
void onBeatDetected()
{
    dbg_str( "Beat!\n");
}
MAX30100 sensorMax;
long meanDiff(int M) {
  #define LM_SIZE 15
  static int LM[LM_SIZE];      // LastMeasurements
//  static byte index = 0;
  static uint8_t index = 0;
  static long sum = 0;
//  static byte count = 0;
  static uint8_t count = 0;
  long avg = 0;

  // keep sum updated to improve speed.
  sum -= LM[index];
  LM[index] = M;
  sum += LM[index];
  index++;
  index = index % LM_SIZE;
  if (count < LM_SIZE) count++;

  avg = sum / count;
  return avg - M;
}

/* User modifiable sensor descriptor in JSON format */
#if (SSI_SENSOR_SELECT_SSSS == 1)

/* BEGIN JSON descriptor for the sensor configuration */

//const char json_string_sensor_config[] = \
//"{"\
//   "\"sample_rate\":100,"\
//   "\"samples_per_packet\":6,"\
//   "\"column_location\":{"\
//	"  \"AccelerometerX\":0,"
//	"  \"AccelerometerY\":1,"
//	"  \"AccelerometerZ\":2"
//   "}"\
//"}\r\n" ;

/* >>>>>>>>>>>>>>>>>>>> ForLPS22HB */
const char json_string_sensor_config[] = \
"{"\
   "\"sample_rate\":100,"\
   "\"samples_per_packet\":2,"\
   "\"column_location\":{"\
	"  \"Pressure\":0,"\
   "}"\
"}\r\n" ;
/* END JSON descriptor for the sensor data */
#endif /* SSI_SENSOR_SELECT_SSSS */

/* User modifiable function. Update the below function
 * to initialize and setup I2C sensors */
void sensor_ssss_configure(void)
{
  /** @todo Replace contents of this function */
  sensor_ssss_config.rate_hz = SENSOR_SSSS_SAMPLE_RATE_HZ;
  sensor_ssss_config.n_channels = SENSOR_SSSS_CHANNELS_PER_SAMPLE;
  sensor_ssss_config.bit_depth = SENSOR_SSSS_BIT_DEPTH;
  sensor_ssss_config.sensor_id = SENSOR_SSSS_ID;
  static int sensor_ssss_configured = false;

  /*--- BEGIN User modifiable section ---*/
//  dbg_str( "qorc_ssi_accel.begin()\n");
  qorc_ssi_accel.begin();
  qorc_ssi_accel.set_sample_rate(sensor_ssss_config.rate_hz);
  qorc_ssi_accel.set_sample_resolution(sensor_ssss_config.bit_depth);
  qorc_ssi_accel.set_mode(MC3635_MODE_CWAKE);
  dbg_str( "qorc_ssi_accel.begin() OK!\n");

  /* >>>>>>>>>>>>>>>>>>>> ForLPS22HB */

  int ret_val;


  /* LPS22HB */
  while (!BARO.begin()) {
	  dbg_str( "BARO.begin() failed!!\n");
	  delay(1000);
  }
  dbg_str( "BARO.begin() OK!\n");

  /* Sparkfun CCS811 */
  while (!SF_CCS811.begin(Wire)) {
  	  dbg_str( "SF_CCS811.begin(Wire) failed!!\n");
  	  delay(1000);
  }
  dbg_str( "SF_CCS811.begin(Wire) OK!\n");

  /* DfRobot CCS811 */
//    while(CCS811.begin() != 0){
//            dbg_str( "CCS811.begin Failed!\n");
//            delay(1000);
//        }
//    dbg_str( "CCS811.begin OK!\n");

  /* MCP9800 */
  MCP9800.begin();
  MCP9800.writeConfig(ADC_RES_12BITS);       // max resolution, 0.0625 °C
  MCP9800.writeTempC2(HYSTERESIS, 0 * 2);    // freezing
  MCP9800.writeTempC2(LIMITSET, 100 * 2);    // boiling
  dbg_str( "MCP9800.begin() OK!\n");

  /* MAX30100 not used */
//  if (!pox.begin()) {
//	  Serial.println("FAILED");
//	  for(;;);
//  } else {
//	  Serial.println("SUCCESS");
//  }

  /* MAX30100 oxullo not used*/
//  	do {
//  	  ret_val = pox.begin(PULSEOXIMETER_DEBUGGINGMODE_RAW_VALUES); // true -> OK!
//  	  if (ret_val == false) {
//  		  sprintf(dbgString1, "pox.begin.begin failed: %d\n", ret_val);
//  		  dbg_str( dbgString1);
//  	  } else {
//  		  dbg_str( "pox.begin.begin OK!\n");
//  	  }
////  	  HAL_DelayUSec(10);
////  	  delay(1000);
//  	} while (ret_val == false);
//
//  // The default current for the IR LED is 50mA and it could be changed
//  //   by uncommenting the following line. Check MAX30100_Registers.h for all the
//  //   available options.
//  // pox.setIRLedCurrent(MAX30100_LED_CURR_7_6MA);
//
//  // Register a callback for the beat detection
//  pox.setOnBeatDetectedCallback(onBeatDetected);

  /* MAX30100 kontakt */
  Wire.begin();
  sensorMax.begin(pw1600, i50, sr100 );
//
  /* AHT10 */
  while (myAHT10.begin() != true) {
      dbg_str("myAHT10.begin() failed!!\n");
      delay(1000);
  }
  dbg_str("myAHT10.begin() OK\n");


  /*--- END of User modifiable section ---*/

  if (sensor_ssss_configured == false)
  {
    sensor_ssss_configured = true;
  }
  sensor_ssss_startstop(1);
}

/* User modifiable function. Update the below function
 * to read sensor data sample from sensors and fill-in
 * the data block with these sensor data values */
int  sensor_ssss_acquisition_buffer_ready()
{
    int8_t *p_dest = (int8_t *) &psensor_ssss_data_block_prev->p_data;
    int dataElementSize = psensor_ssss_data_block_prev->dbHeader.dataElementSize;
    int ret;
    int batch_size;

    p_dest += sizeof(int8_t)*sensor_ssss_samples_collected*dataElementSize;

    /* Read 1 sample per channel, Fill the sample data to p_dest buffer */

    /*--- BEGIN User modifiable section ---*/
    char result[50];
    char result2[50];
    char result3[50];
    static unsigned long lastReadMillis = 0; // variable to save the last executed time
    static unsigned long lastPrintMillis = 0;

    xyz_t accel_data = qorc_ssi_accel.read();  /* Read accelerometer data from MC3635 */

    /* Fill this accelerometer data into the current data block */
    int16_t *p_accel_data = (int16_t *)p_dest;

    *p_accel_data++ = accel_data.x;
    *p_accel_data++ = accel_data.y;
    *p_accel_data++ = accel_data.z;

    p_dest += 6; // advance datablock pointer to retrieve and store next sensor data
//    sprintf(result, "%d", accel_data.x);

    /* -------- Periodic Sensor Readings -------- */
    // Make sure to call update as fast as possible
//    pox.update();
//    float heart_rate = pox.getHeartRate();
//	mxmHeartRate = (int)(heart_rate*100);
//	mxmSpO2 = pox.getSpO2();
//	sprintf(result, "Heart rate: %d, bpm / SpO2: %d\n", mxmHeartRate, mxmSpO2);
//	dbg_str(result);

//    /* For MAX30100 'kontakt' version */
//	sensorMax.readSensor();
//	sprintf(result, "meanDiff(sensorMax.IR): %d\n", meanDiff(sensorMax.IR));
//	dbg_str(result);

	#define SENSOR_READ_INTERVAL 500
	#define DEBUG_PRINT_INTERVAL 2000
    unsigned long currentReadMillis = millis();
    if (currentReadMillis - lastReadMillis >= SENSOR_READ_INTERVAL) {
      lastReadMillis = currentReadMillis; // save the last executed time
		/* >>>>>>>>>>>>>>>>>>>> ForLPS22HB */
		int16_t *p_baro_data = (int16_t *)p_dest;

		float baro_data = BARO.readPressure();
		lpsPressure = (int)(baro_data*100);
		lpsTemperature = BARO.readTemperature();
//		sprintf(dbgString1, "lpsPressure: %d, lpsTemperature: %d\n", lpsPressure, lpsTemperature);
//		dbg_str(dbgString1);

		// Sparkfun
		if (SF_CCS811.dataAvailable()) {
			//If so, have the sensor read and calculate the results.
			//Get them later
			SF_CCS811.readAlgorithmResults();

			//Returns calculated CO2 reading
			ccsEco2 = SF_CCS811.getCO2();
			ccsEtvoc = SF_CCS811.getTVOC();
//			sprintf(dbgString1, "ccsEco2: %d ccsEtvoc: %d\n", ccsEco2, ccsEtvoc);
//			dbg_str(dbgString1);
		}
//	    else {
//	    	dbg_str("SF_CCS811 not available!\n");
//	    }


	//    // DFRobot
	//    if(CCS811.checkDataReady() == true){
	//		ccsEco2 = CCS811.getCO2PPM();
	//		ccsEtvoc = CCS811.getTVOCPPB();
	//		sprintf(result2, "ccsEco2: %d ccsEtvoc: %d\n", ccsEco2, ccsEtvoc);
	//		dbg_str(result2);
	//	} else {
	//		dbg_str("Data is not ready!\n");
	//	}
	//	/*!
	//	 * @brief Set baseline
	//	 * @param get from getBaseline.ino
	//	 */
	//	CCS811.writeBaseLine(0x447B);
	//	//delay cannot be less than measurement cycle
	//	delay(1000);

		/* MCP9800 */
		float body_temp = MCP9800.readTempC16(AMBIENT) / 16.0;
		mcpTemperature = (int)(body_temp*100);
//		sprintf(dbgString1, "mcpTemperature: %d\n", mcpTemperature);
//		dbg_str(dbgString1);
//
//
//		/* MAX30100 */
////		// Make sure to call update as fast as possible
////		pox.update();
//
//		float heart_rate = pox.getHeartRate();
//		mxmHeartRate = (int)(heart_rate*100);
//		mxmSpO2 = pox.getSpO2();
//		sprintf(result, "Heart rate: %d, bpm / SpO2: %d\n", mxmHeartRate, mxmSpO2);
//		dbg_str(result);

    	/* For MAX30100 'kontakt' version */
    	sensorMax.readSensor();
//		sprintf(dbgString1, "meanDiff(sensorMax.IR): %d\n", meanDiff(sensorMax.IR));
//		dbg_str(dbgString1);

    	/* For AHT10 */
		ahtTemperature = (int)(myAHT10.readTemperature()*100);
		ahtRHumidity = (int)(myAHT10.readHumidity()*100);
//		sprintf(dbgString1, "ahtTemperature: %d, ahtRHumidity: %d\n", ahtTemperature, ahtRHumidity);
//		dbg_str(dbgString1);

		/* Populate sensor reading variables for sending to mobile device */
		bodyTemperature = mcpTemperature;
		heartRate 		= mxmHeartRate;
		spO2 			= mxmSpO2;
		ambTemperature 	= (lpsTemperature + ahtTemperature) / 2; // Average the available amb. temp. readings
		relHumidity 	= ahtRHumidity;
		barPressure 	= lpsPressure;
		eCo2 			= ccsEco2;
		eTvoc 			= ccsEtvoc;

		unsigned long currentPrintMillis = millis();
		if (currentPrintMillis - lastPrintMillis >= DEBUG_PRINT_INTERVAL) {
			lastPrintMillis = currentPrintMillis; // save the last executed time
			sprintf(dbgString1, "lpsPressure: %d, lpsTemperature: %d, ", lpsPressure, lpsTemperature);
			dbg_str(dbgString1);

			sprintf(dbgString1, "ccsEco2: %d ccsEtvoc: %d, ", ccsEco2, ccsEtvoc);
			dbg_str(dbgString1);

			sprintf(dbgString1, "mcpTemperature: %d\n", mcpTemperature);
			dbg_str(dbgString1);

			sprintf(dbgString1, "meanDiff(sensorMax.IR): %d, ", meanDiff(sensorMax.IR));
			dbg_str(dbgString1);

			sprintf(dbgString1, "ahtTemperature: %d, ahtRHumidity: %d\n", ahtTemperature, ahtRHumidity);
			dbg_str(dbgString1);
		}
    } /* END: -------- Periodic Sensor Readings -------- */

    /* Read data from other sensors */
    int bytes_to_read = SENSOR_SSSS_CHANNELS_PER_SAMPLE * (SENSOR_SSSS_BIT_DEPTH/8) ;

    sensor_ssss_samples_collected += SENSOR_SSSS_CHANNELS_PER_SAMPLE;
    batch_size = sensor_ssss_batch_size_get() * SENSOR_SSSS_CHANNELS_PER_SAMPLE;

    // return 1 if batch_size of samples are collected
    // return 0 otherwise
    if (sensor_ssss_samples_collected >= batch_size)
    {
      psensor_ssss_data_block_prev->dbHeader.numDataElements = sensor_ssss_samples_collected;
      psensor_ssss_data_block_prev->dbHeader.numDataChannels = SENSOR_SSSS_CHANNELS_PER_SAMPLE;
      sensor_ssss_samples_collected = 0;
      return 1;
    }
    else
    {
      return 0;
    }
}

/** End of User modifiable code and variable */

/*========== BEGIN: SSSS SENSOR Datablock processor definitions =============*/
/** @addtogroup QAI_SSSS_PIPELINE_EXAMPLE QORC SDK SSSS pipeline example
 *
 *  @brief SSSS pipeline example code
 *
 * This example code demonstrates setting up SSSS Queues,
 * setting up the datablock buffer manager (\ref DATABLK_MGR)
 * and setting up the datablock processor processing elements (\ref DATABLK_PE).
 * A specific SSSS processing element for motion detection is provided in this
 * example.
 *
 * @{
 */

/** Maximum number of ssss data blocks that may be queued for chain processing */
#define SENSOR_SSSS_NUM_DATA_BLOCKS              (SENSOR_SSSS_MAX_DATA_BLOCKS)

/** maximum number of vertical (parallel processing elements) that may generate datablock outputs
 *  that may add to the front of the queue.
 *
 *  Queue size of a given datablock processor must be atleast
 *  summation of maximum datablocks of all sensors registered for
 *  processing with some room to handle the vertical depth
 */
#define MAX_THREAD_VERTICAL_DEPTH_DATA_BLOCKS  (5)

#define SENSOR_SSSS_DBP_THREAD_Q_SIZE   (SENSOR_SSSS_MAX_DATA_BLOCKS+MAX_THREAD_VERTICAL_DEPTH_DATA_BLOCKS)
#define SENSOR_SSSS_DBP_THREAD_PRIORITY (10)

uint8_t               sensor_ssss_data_blocks[SENSOR_SSSS_MEMSIZE_MAX] ; //PLACE_IN_SECTION("HWA");
QAI_DataBlockMgr_t    sensor_ssssBuffDataBlkMgr;
QueueHandle_t         sensor_ssss_dbp_thread_q;

/* SSSS AI processing element */
extern void sensor_ssss_ai_data_processor(
       QAI_DataBlock_t *pIn,
       QAI_DataBlock_t *pOut,
       QAI_DataBlock_t **pRet,
       datablk_pe_event_notifier_t *pevent_notifier
     );
extern void sensor_ssss_ai_config(void *pDatablockManagerPtr);
extern int  sensor_ssss_ai_start(void);
extern int  sensor_ssss_ai_stop(void);

/** Sensor SSSS AI processing element functions */

datablk_pe_funcs_t sensor_ssss_sensiml_ai_funcs =
{
  .pconfig     = sensor_ssss_ai_config,
  .pprocess    = sensor_ssss_ai_data_processor,
  .pstart      = sensor_ssss_ai_start,
  .pstop       = sensor_ssss_ai_stop,
  .p_pe_object = (void *)NULL
} ;

/** outQ processor for SSSS AI processing element */
outQ_processor_t sensor_ssss_sensiml_ai_outq_processor =
{
  .process_func = NULL,
  .p_dbm = &sensor_ssssBuffDataBlkMgr,
  .in_pid = SENSOR_SSSS_AI_PID,
  .outQ_num = 0,
  .outQ = NULL,
  .p_event_notifier = NULL
};

/* SSSS Live-stream processing element */
extern void sensor_ssss_livestream_data_processor(
       QAI_DataBlock_t *pIn,
       QAI_DataBlock_t *pOut,
       QAI_DataBlock_t **pRet,
       datablk_pe_event_notifier_t *pevent_notifier
     );
extern void sensor_ssss_livestream_config(void *pDatablockManagerPtr);
extern int  sensor_ssss_livestream_start(void);
extern int  sensor_ssss_livestream_stop(void);

/** Sensor SSSS AI processing element functions */

datablk_pe_funcs_t sensor_ssss_livestream_funcs =
{
  .pconfig     = sensor_ssss_livestream_config,
  .pprocess    = sensor_ssss_livestream_data_processor,
  .pstart      = sensor_ssss_livestream_start,
  .pstop       = sensor_ssss_livestream_stop,
  .p_pe_object = (void *)NULL
} ;

/** outQ processor for SSSS Live-stream processing element */
outQ_processor_t sensor_ssss_livestream_outq_processor =
{
  .process_func = NULL,
  .p_dbm = &sensor_ssssBuffDataBlkMgr,
  .in_pid = SENSOR_SSSS_LIVESTREAM_PID,
  .outQ_num = 0,
  .outQ = NULL,
  .p_event_notifier = NULL
};

/* SSSS datasave processing element */
extern void sensor_ssss_datasave_data_processor(
       QAI_DataBlock_t *pIn,
       QAI_DataBlock_t *pOut,
       QAI_DataBlock_t **pRet,
       datablk_pe_event_notifier_t *pevent_notifier
     );
extern void sensor_ssss_datasave_config(void *pDatablockManagerPtr);
extern int  sensor_ssss_datasave_start(void);
extern int  sensor_ssss_datasave_stop(void);

/** Sensor SSSS datasave processing element functions */

datablk_pe_funcs_t sensor_ssss_datasave_funcs =
{
  .pconfig     = sensor_ssss_datasave_config,
  .pprocess    = sensor_ssss_datasave_data_processor,
  .pstart      = sensor_ssss_datasave_start,
  .pstop       = sensor_ssss_datasave_stop,
  .p_pe_object = (void *)NULL
} ;

/** outQ processor for SSSS datasave processing element */
outQ_processor_t sensor_ssss_datasave_outq_processor =
{
  .process_func = NULL,
  .p_dbm = &sensor_ssssBuffDataBlkMgr,
  .in_pid = SENSOR_SSSS_LIVESTREAM_PID,
  .outQ_num = 0,
  .outQ = NULL,
  .p_event_notifier = NULL
};

datablk_pe_descriptor_t  sensor_ssss_datablk_pe_descr[] =
{ // { IN_ID, OUT_ID, ACTIVE, fSupplyOut, fReleaseIn, outQ, &pe_function_pointers, bypass_function, pe_semaphore }
#if (SENSOR_SSSS_RECOG_ENABLED)
    /* processing element descriptor for SensiML AI for SSSS sensor */
    { SENSOR_SSSS_ISR_PID, SENSOR_SSSS_AI_PID, true, false, true, &sensor_ssss_sensiml_ai_outq_processor, &sensor_ssss_sensiml_ai_funcs, NULL, NULL},
#endif

#if (SENSOR_SSSS_LIVESTREAM_ENABLED)
    /* processing element descriptor for SSSS sensor livestream */
    { SENSOR_SSSS_ISR_PID, SENSOR_SSSS_LIVESTREAM_PID, true, false, true, &sensor_ssss_livestream_outq_processor, &sensor_ssss_livestream_funcs, NULL, NULL},
#endif

#if (SENSOR_SSSS_DATASAVE_ENABLED)
    /* processing element descriptor for SSSS sensor datasave */
    { SENSOR_SSSS_ISR_PID, SENSOR_SSSS_DATASAVE_PID, true, false, true, &sensor_ssss_datasave_outq_processor, &sensor_ssss_datasave_funcs, NULL, NULL},
#endif

};

datablk_processor_params_t sensor_ssss_datablk_processor_params[] = {
    { SENSOR_SSSS_DBP_THREAD_PRIORITY,
      &sensor_ssss_dbp_thread_q,
      sizeof(sensor_ssss_datablk_pe_descr)/sizeof(sensor_ssss_datablk_pe_descr[0]),
      sensor_ssss_datablk_pe_descr,
      256*2,
      (char*)"SENSOR_SSSS_DBP_THREAD",
      NULL
    }
};

void sensor_ssss_block_processor(void)
{
  /* Initialize datablock manager */
   datablk_mgr_init( &sensor_ssssBuffDataBlkMgr,
                      sensor_ssss_data_blocks,
                      sizeof(sensor_ssss_data_blocks),
                      (SENSOR_SSSS_SAMPLES_PER_BLOCK),
                      ((SENSOR_SSSS_BIT_DEPTH)/8)
                    );

  /** SSSS datablock processor thread : Create SSSS Queues */
  sensor_ssss_dbp_thread_q = xQueueCreate(SENSOR_SSSS_DBP_THREAD_Q_SIZE, sizeof(QAI_DataBlock_t *));
  vQueueAddToRegistry( sensor_ssss_dbp_thread_q, "SENSOR_SSSSPipelineExampleQ" );

  /** SSSS datablock processor thread : Setup SSSS Thread Handler Processing Elements */
  datablk_processor_task_setup(&sensor_ssss_datablk_processor_params[0]);

  /** Set the first data block for the ISR or callback function */
  sensor_ssss_set_first_data_block();

  /* [TBD]: sensor configuration : should this be here or after scheduler starts? */
  sensor_ssss_add();
  sensor_ssss_configure();
#if 0
  printf("Sensor Name:                   %s\n", "SENSOR_SSSS_NAME");
  printf("Sensor Memory:                 %d\n", (int)SENSOR_SSSS_MEMSIZE_MAX);
  printf("Sensor Sampling rate:          %d Hz\n", (int)SENSOR_SSSS_SAMPLE_RATE_HZ);
  printf("Sensor Number of channels:     %d\n", (int)SENSOR_SSSS_CHANNELS_PER_SAMPLE);
  printf("Sensor frame size per channel: %d\n", (int)SENSOR_SSSS_SAMPLES_PER_CHANNEL);
  printf("Sensor frame size:             %d\n", (int)SENSOR_SSSS_SAMPLES_PER_BLOCK);
  printf("Sensor sample bit-depth:       %d\n", (int)SENSOR_SSSS_BIT_DEPTH);
  printf("Sensor datablock count:        %d\n", (int)SENSOR_SSSS_NUM_DATA_BLOCKS);
#endif
}
/*========== END: SSSS SENSOR Datablock processor definitions =============*/

/* BEGIN timer task related functions */
TimerHandle_t sensor_ssss_TimId ;
extern "C" void sensor_ssss_dataTimer_Callback(TimerHandle_t hdl);
void sensor_ssss_dataTimer_Callback(TimerHandle_t hdl)
{
  // Warning: must not call vTaskDelay(), vTaskDelayUntil(), or specify a non zero
  // block time when accessing a queue or a semaphore.
  sensor_ssss_acquisition_read_callback(); //osSemaphoreRelease(readDataSem_id);
}
void sensor_ssss_dataTimerStart(void)
{
  BaseType_t status;
  TimerCallbackFunction_t xCallback = sensor_ssss_dataTimer_Callback;
#if (USE_SENSOR_SSSS_FIFO_MODE)
  // setup FIFO mode
#else
  int milli_secs = (1000 / SENSOR_SSSS_SAMPLE_RATE_HZ); // reads when a sample is available (upto 416Hz)
#endif

  // Create periodic timer
  if (!sensor_ssss_TimId) {
    sensor_ssss_TimId = xTimerCreate("SensorSSSSTimer", pdMS_TO_TICKS(milli_secs), pdTRUE, (void *)0, xCallback);
    configASSERT(sensor_ssss_TimId != NULL);
  }

  if (sensor_ssss_TimId)  {
    status = xTimerStart (sensor_ssss_TimId, 0);  // start timer
    if (status != pdPASS)  {
      // Timer could not be started
    }
  }
#if (USE_SENSOR_SSSS_FIFO_MODE)
  // setup FIFO mode
#endif
  // start the sensor
}

void sensor_ssss_dataTimerStop(void)
{
  if (sensor_ssss_TimId) {
    xTimerStop(sensor_ssss_TimId, 0);
  }
  // stop the sensor
}

/* END timer task related functions */

/* BEGIN Sensor Generic Configuration */

sensor_generic_config_t sensor_ssss_config;

void sensor_ssss_startstop( int is_start )
{
  /** @todo Replace contents of this function */
  if ((is_start) && (sensor_ssss_config.enabled) && (sensor_ssss_config.is_running == 0) )
  {
     sensor_ssss_dataTimerStart();
     sensor_ssss_config.is_running = 1;
  }
  else if ( (is_start == 0) && (sensor_ssss_config.is_running == 1) )
  {
    sensor_ssss_dataTimerStop();
    sensor_ssss_config.is_running = 0;
  }
}

void sensor_ssss_clear( void )
{
  sensor_ssss_config.enabled = false;
  /** @todo Replace contents of this function */
}

void sensor_ssss_add(void)
{
  sensor_ssss_config.enabled = true;
  /** @todo Replace contents of this function */
}
/* End of Sensor Generic Configuration */

/* BEGIN SSSS Acquisition */
/* Sensor SSSS capture ISR */
#define SENSOR_SSSS_ISR_EVENT_NO_BUFFER  (1)   ///< error getting a new datablock buffer

#define SSSS_ISR_OUTQS_NUM        (1)
QueueHandle_t   *sensor_ssss_isr_outQs[SSSS_ISR_OUTQS_NUM] = { &sensor_ssss_dbp_thread_q };
QAI_DataBlock_t *psensor_ssss_data_block_prev = NULL;
int              sensor_ssss_samples_collected = 0;

outQ_processor_t sensor_ssss_isr_outq_processor =
{
  .process_func = sensor_ssss_acquisition_read_callback,
  .p_dbm = &sensor_ssssBuffDataBlkMgr,
  .in_pid = SENSOR_SSSS_ISR_PID,
  .outQ_num = 1,
  .outQ = sensor_ssss_isr_outQs,
  .p_event_notifier = NULL
};

void sensor_ssss_set_first_data_block()
{
  /* Acquire a datablock buffer */
  if (NULL == psensor_ssss_data_block_prev)
  {
    datablk_mgr_acquire(sensor_ssss_isr_outq_processor.p_dbm, &psensor_ssss_data_block_prev, 0);
  }
  configASSERT(psensor_ssss_data_block_prev); // probably indicates uninitialized datablock manager handle
  sensor_ssss_samples_collected = 0;
  psensor_ssss_data_block_prev->dbHeader.Tstart = xTaskGetTickCount();
}

int sensor_ssss_batch_size_get(void)
{
  return (SENSOR_SSSS_SAMPLES_PER_CHANNEL);
}

void sensor_ssss_acquisition_read_callback(void)
{
    int gotNewBlock = 0;
    QAI_DataBlock_t  *pdata_block = NULL;

    if (!sensor_ssss_acquisition_buffer_ready())
    {
      return;
    }
    /* Acquire a new data block buffer */
    datablk_mgr_acquire(sensor_ssss_isr_outq_processor.p_dbm, &pdata_block, 0);
    if (pdata_block)
    {
        gotNewBlock = 1;
    }
    else
    {
        // send error message
        // xQueueSendFromISR( error_queue, ... )
        if (sensor_ssss_isr_outq_processor.p_event_notifier)
          (*sensor_ssss_isr_outq_processor.p_event_notifier)(sensor_ssss_isr_outq_processor.in_pid, SENSOR_SSSS_ISR_EVENT_NO_BUFFER, NULL, 0);
        pdata_block = psensor_ssss_data_block_prev;
        pdata_block->dbHeader.Tstart = xTaskGetTickCount();
        pdata_block->dbHeader.numDropCount++;
    }

    if (gotNewBlock)
    {
        /* send the previously filled ssss data to specified output Queues */
        psensor_ssss_data_block_prev->dbHeader.Tend = pdata_block->dbHeader.Tstart;
        datablk_mgr_WriteDataBufferToQueues(&sensor_ssss_isr_outq_processor, NULL, psensor_ssss_data_block_prev);
        psensor_ssss_data_block_prev = pdata_block;
    }
}
/* END SSSS Acquisition */

/* SSSS AI processing element functions */
void sensor_ssss_ai_data_processor(
       QAI_DataBlock_t *pIn,
       QAI_DataBlock_t *pOut,
       QAI_DataBlock_t **pRet,
       datablk_pe_event_notifier_t *pevent_notifier
     )
{
    int16_t *p_data = (int16_t *) ( (uint8_t *)pIn + offsetof(QAI_DataBlock_t, p_data) );

    // Invoke the SensiML recognition API
    int nSamples = pIn->dbHeader.numDataElements;
    int nChannels = pIn->dbHeader.numDataChannels;
    set_recognition_current_block_time();
    int batch_sz = nSamples / nChannels;
    int classification = sml_recognition_run_batch(p_data, batch_sz, nChannels, sensor_ssss_config.sensor_id);
    *pRet = NULL;
    return;
}

void sensor_ssss_ai_config(void *pobj)
{
}

int  sensor_ssss_ai_start(void)
{
  return 0;
}

int  sensor_ssss_ai_stop(void)
{
  return 0;
}

void sensor_ssss_event_notifier(int pid, int event_type, void *p_event_data, int num_data_bytes)
{
  char *p_data = (char *)p_event_data;
  printf("[SSSS Event] PID=%d, event_type=%d, data=%02x\n", pid, event_type, p_data[0]);
}

#if (SENSOR_COMMS_KNOWN_PATTERN == 1)
int16_t sensor_ssss_debug_buffer[240]; // Buffer intended to send a known pattern such as sawtooth
int16_t sensor_ssss_debug_data = 0;    // state for holding current data for the known pattern
#endif

/* SSSS livestream processing element functions */

void sensor_ssss_livestream_data_processor(
       QAI_DataBlock_t *pIn,
       QAI_DataBlock_t *pOut,
       QAI_DataBlock_t **pRet,
       datablk_pe_event_notifier_t *pevent_notifier
     )
{
    int16_t *p_data = (int16_t *) ( (uint8_t *)pIn + offsetof(QAI_DataBlock_t, p_data) );
    //struct sensor_data sdi;
    uint64_t  time_start, time_curr, time_end, time_incr;

    if (sensor_ssss_config.enabled == true)
    {
      // Live-stream data to the host
      uint8_t *p_source = pIn->p_data ;
      int ilen = pIn->dbHeader.numDataElements * pIn->dbHeader.dataElementSize ;
#if (SENSOR_SSSS_RATE_DEBUG_GPIO == 1)
      // Toggle GPIO to indicate that a new datablock buffer is dispatched to UART
      // for transmission for data collection
      HAL_GPIO_Write(GPIO_2, sensor_rate_debug_gpio_val);
      sensor_rate_debug_gpio_val ^= 1;
#endif

#if (SENSOR_COMMS_KNOWN_PATTERN == 1)
      int nSamples = pIn->dbHeader.numDataElements;
      int nChannels = pIn->dbHeader.numDataChannels;
      // prepare the sawtooth known pattern data
      for (int k = 0; k < nSamples; k+=nChannels)
      {
    	  for (int l = 0; l < nChannels; l++)
              sensor_ssss_debug_buffer[k+l] = sensor_ssss_debug_data;
    	  sensor_ssss_debug_data++;
      }
	  memcpy (pIn->p_data, sensor_ssss_debug_buffer, nSamples * sizeof(int16_t));
#endif
      ssi_publish_sensor_data(p_source, ilen);
    }
    *pRet = NULL;
    return;
}

void sensor_ssss_livestream_config(void *pobj)
{
}

int  sensor_ssss_livestream_start(void)
{
  return 0;
}

int  sensor_ssss_livestream_stop(void)
{
  return 0;
}

void sensor_ssss_datasave_data_processor(
       QAI_DataBlock_t *pIn,
       QAI_DataBlock_t *pOut,
       QAI_DataBlock_t **pRet,
       datablk_pe_event_notifier_t *pevent_notifier
     )
{
    int16_t *p_data = (int16_t *) ( (uint8_t *)pIn + offsetof(QAI_DataBlock_t, p_data) );
    //struct sensor_data sdi;
    uint64_t  time_start, time_curr, time_end, time_incr;

    if (sensor_ssss_config.enabled == true)
    {
      // Live-stream data to the host
      uint8_t *p_source = pIn->p_data ;
      int ilen = pIn->dbHeader.numDataElements * pIn->dbHeader.dataElementSize ;
      /* Save data to the */
      struct sensor_data Info, *pInfo = &Info;
      pInfo->bytes_per_reading = pIn->dbHeader.dataElementSize;
      pInfo->n_bytes = ilen;
      pInfo->rate_hz = sensor_ssss_config.rate_hz;
      pInfo->sensor_id = sensor_ssss_config.sensor_id;
      pInfo->time_end = convert_to_uSecCount(pIn->dbHeader.Tend);
      pInfo->time_start = convert_to_uSecCount(pIn->dbHeader.Tstart);
      pInfo->vpData = p_source;
      data_save((const struct sensor_data *)pInfo);

      }
    *pRet = NULL;
    return;
}

void sensor_ssss_datasave_config(void *pobj)
{
}

int  sensor_ssss_datasave_start(void)
{
  return 0;
}

int  sensor_ssss_datasave_stop(void)
{
  return 0;
}

