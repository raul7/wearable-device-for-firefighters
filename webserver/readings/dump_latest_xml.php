<?php
    // Change to your preferred time zone
	date_default_timezone_set("America/La_Paz");

	////// --------------------------------------------------------------
	header("Content-type: text/xml");

	// Start XML file, echo parent node
	echo "<?xml version='1.0' ?>";
	echo '<fighters>';

	if ($handle = opendir('data')) {

	    while (false !== ($entry = readdir($handle))) {

	        if ($entry != "." && $entry != "..") {

	            // echo "$entry\n";

	            ////// -------------- Read one file and dump XML ------------
				// $file_contents_string = file_get_contents('Raul Alvarez.txt');
				$file_contents_string = file_get_contents('data/' . $entry);
				$file_contents_array = explode (",", $file_contents_string);
				$file_contents_array_2 = explode ("{", $file_contents_string);

				// echo $file_contents_string . "..."; // For debugging
				// print_r($file_contents_array);

				$unix_time =   $file_contents_array[1];
				$name =   $file_contents_array[2];
				$id_code =   $file_contents_array[3];
				$latitude =   $file_contents_array[4];
				$longitude =   $file_contents_array[5];
				$status =   $file_contents_array[6];
				$sensors =   "{" . $file_contents_array_2[1];
				// echo "sensors: $sensors<br>";

				// Replace double quotes in the 'sensors' JSON string with ' &quot;' to include the string as valid XML data
	            $replaced = str_replace("\"", "&quot;", $sensors);

				// Add to XML document node
				echo '<fighter ';
				echo 'unix_time="' . $unix_time . '" ';
				echo 'name="' . $name . '" ';
				echo 'id_code="' . $id_code . '" ';
				echo 'latitude="' . $latitude . '" ';
				echo 'longitude="' . $longitude . '" ';
				echo 'status="' . $status . '" ';
				echo 'sensors="' . $replaced . '"';
				echo '/>';
	        }
	    }

	    closedir($handle);
	}

	// End XML file
	echo '</fighters>';
	
?>