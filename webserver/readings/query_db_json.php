<?php
	// Connect to database
	require_once 'login.php'; // Don't forget to change your database login setting in the 'login.php' file

	$limit = 30; // Number of DB entries to include in JSON file
    // $name = 'Raul Alvarez';
    $name = '';
    if (isset($_GET['name'])) {
        $name = $_GET['name'];
        // echo "name is $name <br>";
    }

	$conn = new mysqli($hn, $un, $pw, $db);
	if ($conn->connect_error) die($conn->connect_error);

    // Build DB query
	// $query  = "SELECT * FROM (SELECT * FROM distancing ORDER BY id DESC LIMIT " . $limit . ") sub ORDER BY id ASC"; 
    // $query  = "SELECT * FROM (SELECT sensors FROM wff_readings WHERE name='$name' ORDER BY id DESC LIMIT " . $limit . ") sub ORDER BY id ASC";
    $query  = "SELECT sensors FROM wff_readings WHERE name='$name' ORDER BY id DESC LIMIT " . $limit;

    // Send DB query
	$result = $conn->query($query);

	$to_encode = array();
    $data_array = array();

	if (!$result) die($conn->error); // If the query fails

	$rows = $result->num_rows; // Get number of records

	$min_idx = 0; // Get all records starting with the first one

	// Iterate over records
	for ($j = $min_idx ; $j < $rows ; ++$j)
	{
		$result->data_seek($j); // Move pointer to first record
        // echo $result;
		$row = $result->fetch_array(MYSQLI_ASSOC); // Get record
        // print_r($row[sensors]);
		$to_encode[] = $row[sensors];
	}

	// Close connection to DB
	$result->close();
	$conn->close();

	echo json_encode($to_encode);
?>

