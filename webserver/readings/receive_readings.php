<?php
	$DEBUG = True;

    // Default time zone
	date_default_timezone_set("America/La_Paz");

    // Connect to database
    require_once 'login.php'; // Don't forget to change your database login setting in the 'login.php' file
    
    $conexion = new mysqli($hn, $un, $pw, $db);
    if ($conexion->connect_error) die($conexion->connect_error);
    echo "Connected to DB!";

	// Send a message to the client
    $nombre_servidor = gethostname();
    $ip_servidor = $_SERVER['SERVER_ADDR'];  //getHostByName(getHostName());
    echo "Received POST data at: " . $nombre_servidor . " " . $ip_servidor . " !\n"; // Just for debugging
	
	//// --------- Store data from received HTTP POST in text file ------------------------------
	if (isset($_POST['unix_time']) && isset($_POST['name']) && isset($_POST['id_code']) &&
      isset($_POST['latitude']) && isset($_POST['longitude']) && isset($_POST['status']) && isset($_POST['sensors'])) {
	    // 
    	$unix_time =   $_POST["unix_time"];
    	$name =   $_POST["name"];
    	$id_code =   $_POST["id_code"];
    	$latitude =   $_POST["latitude"];
    	$longitude =   $_POST["longitude"];
    	$status =   $_POST["status"];
    	$sensors =   $_POST["sensors"];
        echo "Received sensors: " . $sensors . "\n";
        echo "unix_time: $unix_time\n";
    	
        // Create artificial data for easy debugging
        if ($DEBUG == True) {
            $name =   "Raul Alvarez";
            $rlat = rand(-100,100) / 100000.0;
            $latitude = $latitude + $rlat;
            $rlon = rand(-100,100) / 100000.0;
            $longitude = $longitude + $rlon;
        }

    	// Make data string to store in text file
    	$data =  date('D d M Y H:i:s', $unix_time) . ',' . $unix_time . ',' . $name . ',' . $id_code . ',' . $latitude . ','  . $longitude . ',' . $status. ',' . $sensors;
    	
        // 2021-05-12 15:21:53
        $formatted_date = date('Y-m-d H:i:s', $unix_time);
        echo "formatted_date: $formatted_date \n";
    	// Store data in a text file
        $file =  './data/'. str_replace(' ', '_', $name) . '.txt';
    	// file_put_contents($file, $data, FILE_APPEND | LOCK_EX); // JUST FOR DEBUGGING: For appending consecutive data
    	file_put_contents($file, $data, LOCK_EX);

    	// Build DB query
        $query    = "INSERT INTO wff_readings(unix_time, name, id_code, latitude, longitude, status, sensors)
 VALUES('$formatted_date', '$name', '$id_code', '$latitude', '$longitude', '$status', '$sensors')";

        // Send DB query
        $result   = $conexion->query($query);

        // If the query fails
        if (!$result) echo "INSERT failed: $query<br>" .
          $conexion->error . "<br><br>";
        else echo "Data inserted into DB!\r\n";
	}
	else {
	    // Send a message to the client
	    echo "One or more data pairs weren't received...\n"; // Just for debugging
	}
?>
