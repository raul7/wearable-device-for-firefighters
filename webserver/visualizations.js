/* --- Wearable Device for Firefighters */
var customLabel = {
  fire_fighter: {
    label: 'F'
  },
  fire_position: { /* Not used yet */
    label: 'P'
  },
  drone_position: { /* Not used yet */
    label: 'D'
  }
};

var statusIcon = {
  normal: {
    url: 'https://maps.google.com/mapfiles/ms/icons/green-dot.png'
  },
  help: {
    url: 'https://maps.google.com/mapfiles/ms/icons/red-dot.png'
  }
};


// Alalay lake
// var initial_main_latitude = -17.417266;
// var initial_main_longitude = -66.132659;

// Home
var initial_main_latitude = -17.420385;
var initial_main_longitude = -66.1643971;

var fighter_marker;
var fighter_marker_array = [];
var reading_circle_array = [];
var heatmap_array = [];
var fighter_name_array = [];

var google_map;
var image;

var is_initializing = true;

var sel_sensor_index = null;
var sel_crew_member_name = "";
var sel_crew_member_idx = null;
var task_cmd_post_data = {};
var sel_sensor_index = 1;
var sel_sensor_name = "eco2"; // Dafault sensor data to display with heatmap and the sensor graph

var main_latitude = null;
var main_longitude = null;

var weather_get_alternation = true;
var graph_is_starting = true;

// Icon size and html tags
var icon_size = 25;
var firefighter_icon_tag = "<img src=\"icons/firefighter.png\" width=\"" + icon_size + "\" height=\"" + icon_size + "\">";
var amb_temp_icon_tag = "<img src=\"icons/amb_temp.png\" width=\"" + icon_size + "\" height=\"" + icon_size + "\">";
var body_temp_icon_tag = "<img src=\"icons/body_temp.png\" width=\"" + icon_size + "\" height=\"" + icon_size + "\">";
var heart_rate_icon_tag = "<img src=\"icons/heart_rate.png\" width=\"" + icon_size + "\" height=\"" + icon_size + "\">";
var bar_press_icon_tag = "<img src=\"icons/bar_press.png\" width=\"" + icon_size + "\" height=\"" + icon_size + "\">";
var rel_hum_icon_tag = "<img src=\"icons/rel_hum.png\" width=\"" + icon_size + "\" height=\"" + icon_size + "\">";
var spo2_icon_tag = "<img src=\"icons/spo2.png\" width=\"" + icon_size + "\" height=\"" + icon_size + "\">";
var eco2_icon_tag = "<img src=\"icons/eco2.png\" width=\"" + icon_size + "\" height=\"" + icon_size + "\">";
var etvoc_icon_tag = "<img src=\"icons/etvoc.png\" width=\"" + icon_size + "\" height=\"" + icon_size + "\">";

var weather_icon_tag = "<img src=\"icons/weather.png\" width=\"" + icon_size + "\" height=\"" + icon_size + "\">";
var weather_forecast_icon_tag = "<img src=\"icons/weather_forecast.png\" width=\"" + icon_size + "\" height=\"" + icon_size + "\">";

// Labels for detected activities by the ML recognition model in the QuickFeather
var activity_class_dict = { 1: "idle", 2: "using_ax", 3: "using_chainsaw", 4: "walking", 0: "Unknown" };

//////// --------------- Init Google map ---------------
function initMap() {
  console.log("------ Hi there! -------")
  console.log("Thanks for reviewing my code. Have fun!")
  google_map = new google.maps.Map(document.getElementById('map'), {
    center: new google.maps.LatLng(initial_main_latitude, initial_main_longitude),
    mapTypeId: 'terrain', // 'satellite',
    zoom: 18
  });
  fighter_name_array = []; // Flush to prevent chached data
  heatmap = new google.maps.visualization.HeatmapLayer();

  main_latitude = initial_main_latitude;
  main_longitude = initial_main_longitude;

  refreshMapCallback();
  setInterval(refreshMapCallback, 3000);
  setInterval(getCurrentWeather, 3000);
  setInterval(getWeatherForecast, 5000);

  function refreshMapCallback() {
    console.log('refreshMapCallback timeout!');
    ////// -------- Refresh fire fighter data ---------------------------
    downloadUrl('readings/dump_latest_xml.php', function (data) {
      var xml = data.responseXML;
      refreshReadings(xml);
    });
  }
}

function refreshReadings(xml) {
  var infoWindow = new google.maps.InfoWindow;
  var fighters = xml.documentElement.getElementsByTagName('fighter');

  // Delete all previous fire fighter markers
  fa_len = fighter_marker_array.length;
  for (i = 0; i < fa_len; i++) {
    var marker = fighter_marker_array[i];
    if (marker) {
      marker.setMap(null);
      marker = null;
    }
  }

  // Delete all fire fighter marker references
  fighter_marker_array = [];
  var fighter_index = 0;

  // Delete all previous reading circles
  ca_len = reading_circle_array.length;
  for (i = 0; i < ca_len; i++) {
    var circle = reading_circle_array[i];
    if (circle) {
      circle.setMap(null);
      circle = null;
    }
  }
  // Delete all fire fighter marker references
  reading_circle_array = [];
  var circle_index = 0;

  heatmap.setMap(null);
  heatmap_array = [];
  var heatmap_index = 0;

  // Draw markers for all firefighters
  Array.prototype.forEach.call(fighters, function (fighterElem) {
    var id_code = fighterElem.getAttribute('id_code');
    var name = fighterElem.getAttribute('name');
    var unix_time = fighterElem.getAttribute('unix_time');
    var status = fighterElem.getAttribute('status');
    // console.log("status: " + status);
    // var type = fighterElem.getAttribute('type');
    var type = 'fire_fighter';
    var latitude = parseFloat(fighterElem.getAttribute('latitude'));
    var longitude = parseFloat(fighterElem.getAttribute('longitude'));
    var sensors = fighterElem.getAttribute('sensors');
    // console.log("sensors: " + sensors);

    // Converting JSON-encoded string to JS object
    var obj = JSON.parse(sensors);
    // console.log("Sensor vals c: " + obj.c);

    var point = new google.maps.LatLng(latitude, longitude);

    var text = document.createElement('text');

    var infowincontent = document.createElement('div');
    var strong = document.createElement('strong');
    strong.textContent = "Name: " + name;
    infowincontent.appendChild(strong);
    infowincontent.appendChild(document.createElement('br'));

    var text = document.createElement('text');
    // Show data rounding decimals. Convert first from string to float
    text.textContent = "[ Status: " + status + '\n'
      + " ][ ID: " + id_code + "\n"
      + " ][ Lat: " + parseFloat(latitude).toFixed(5) + '\n'
      + " ][ Lon: " + parseFloat(longitude).toFixed(5) + " ]"
      ;
    infowincontent.appendChild(text);

    infowincontent.appendChild(document.createElement('br'));
    var strong = document.createElement('strong');
    strong.textContent = "Sensors: ";
    infowincontent.appendChild(strong);
    infowincontent.appendChild(document.createElement('br'));

    var sensors_text = document.createElement('text');
    // Set firefighter data in the corresponding infoWindow
    sensors_text.textContent = "[ Activ: " + activity_class_dict[obj.class]
      + " ][ Body (°C): " + obj.body_temp
      + " ][ HRate (BPM): " + obj.heart_rate
      + " ][ SPO2 (%): " + obj.spo2
      + " ][ Amb (°C): " + obj.amb_temp
      + " ][ RH (%): " + obj.rel_hum
      + " ][ BP (KPa): " + obj.bar_press
      + " ][ eCO2 (PPM): " + obj.eco2
      + " ][ eTVOC (PPB): " + obj.etvoc + " ]"
      ;
    infowincontent.appendChild(sensors_text);

    var icon = customLabel[type] || {};

    var icon_source = statusIcon[status] || {};
    // console.log('icon_source.url', icon_source.url)

    var fighter_marker = new google.maps.Marker({
      map: google_map,
      position: point,
      // icon: {url: "https://maps.google.com/mapfiles/ms/icons/green-dot.png",
      //         scaledSize: new google.maps.Size(50, 50), // scaled size
      //       },
      icon: {
        url: icon_source.url,
        scaledSize: new google.maps.Size(50, 50), // scaled size
      },
      label: icon.label
    });
    fighter_marker.addListener('click', function () {
      infoWindow.setContent(infowincontent);
      infoWindow.open(google_map, fighter_marker);
    });
    fighter_marker_array[fighter_index] = fighter_marker;
    fighter_index = fighter_index + 1;

    // Update sensor heatmap
    updateSensorHeatmap(obj, point, heatmap_index);
    heatmap_index = heatmap_index + 1;

    //// ---------------- Update list of detected firefighters and selected firefighter data ---------------- 
    // Update list of currently detected firefighters in the system
    var select = document.getElementById("crew_member");
    if (fighter_name_array.includes(name) == false) {
      fighter_name_array.push(name);
      var opt = name;
      var el = document.createElement("option");
      el.textContent = opt;
      el.value = opt;
      // If the page is loading for the first time, make the first name as 'selected'
      if (window.graph_is_starting == true) {
        // console.log("sel_crew_member_name is empty");
        el.selected = "selected";
      }
      // el.selected = "selected";
      select.appendChild(el);
    }
    // Update data from the current selected firefighter in the text field below the map
    if (window.sel_crew_member_name != "") {
      if (window.sel_crew_member_name == name) {
        document.getElementById('selected_fighter_data').innerHTML = "<strong>Current Selected Fighter: </strong>"
          + name + "<br>"
          + "[ " + firefighter_icon_tag +  "Activ: " + activity_class_dict[obj.class]
          // + " ][ <img src=\"https://tec.bo/quicklogic/iconfinder_weather-01_1530392.png\" width=\"25\" height=\"25\">Body (°C): " + obj.body_temp
          + " ][ " + body_temp_icon_tag +  "Body (°C): " + obj.body_temp
          + " ][ " + heart_rate_icon_tag +  "HRate (BPM): " + obj.heart_rate
          + " ][ " + spo2_icon_tag +  "SPO2 (%): " + obj.spo2
          + " ][ "+ amb_temp_icon_tag + "Amb (°C): " + obj.amb_temp
          + " ][ " + rel_hum_icon_tag +  "RH (%): " + obj.rel_hum
          + " ][ " + bar_press_icon_tag +  "BP (KPa): " + obj.bar_press
          + " ][ " + eco2_icon_tag +  "eCO2 (PPM): " + obj.eco2
          + " ][ " + etvoc_icon_tag +  "eTVOC (PPB): " + obj.etvoc + " ]"
          ;
      }
    }

    if (window.graph_is_starting == true) {
      window.graph_is_starting = false;
      var crewMemberDropdown = document.getElementById("crew_member");
      window.sel_crew_member_name = crewMemberDropdown.options[crewMemberDropdown.selectedIndex].text;
    }
    refreshGraph();

  });

  heatmap = new google.maps.visualization.HeatmapLayer({
    data: heatmap_array,
    dissipating: true, // default value is true
    // maxIntensity: 10,
    // opacity: 1.0,
    radius: 70,
  });
  heatmap.setMap(google_map);
} // END refreshReadings(xml)

function downloadUrl(url, callback) {
  var request = window.ActiveXObject ?
    new ActiveXObject('Microsoft.XMLHTTP') :
    new XMLHttpRequest;

  request.onreadystatechange = function () {
    if (request.readyState == 4) {
      request.onreadystatechange = doNothing;
      callback(request, request.status);
    }
  };

  request.open('GET', url, true);
  request.send(null);
} // END: initMap

////// -------- Update sensor graph  ---------------------------
function refreshGraph() {

  var body_temp_array = [];
  var heart_rate_array = [];
  var spo2_array = [];
  var amb_temp_array = [];
  var rel_hum_array = [];
  var bar_pres_array = [];
  var eco2_array = [];
  var etvoc_array = [];

  console.log('Updating timeline');
  $.getJSON('readings/query_db_json.php?name=' + sel_crew_member_name, function (data) {
    // console.log('data: ', data);
    $.each(data, function (fieldName, fieldValue) {
      // console.log('fieldName: ', fieldName);
      // console.log('fieldValue: ', fieldValue);

      var jsonStr = JSON.parse(fieldValue);
      // console.log('jsonStr.body_temp: ', jsonStr.body_temp);

      body_temp_array.push(jsonStr.body_temp);
      heart_rate_array.push(jsonStr.heart_rate);
      spo2_array.push(jsonStr.spo2);
      amb_temp_array.push(jsonStr.amb_temp);
      rel_hum_array.push(jsonStr.rel_hum);
      bar_pres_array.push(jsonStr.bar_pres);
      eco2_array.push(jsonStr.eco2 / 10); // Scaled down by 10 just for visualization purposes
      etvoc_array.push(jsonStr.etvoc / 10); // Scaled down by 10 just for visualization purposes
    });
    // console.log('body_temp_array: ', body_temp_array);

    // TODO: play with the 'fill' type parameter for better visualization
    var body_temp_graph = {
      y: body_temp_array,
      type: 'line',
      // fill: 'tonexty',
      name: 'Body [°C]'
    };

    var heart_rate_graph = {
      y: heart_rate_array,
      type: 'line',
      // fill: 'tonexty',
      name: 'Heart Rate [BPM]'
    };

    var spo2_graph = {
      y: spo2_array,
      type: 'line',
      fill: 'tozeroy', // fill: 'tonexty',
      name: 'SPO2 [%]'
    };

    var amb_temp_graph = {
      y: amb_temp_array,
      type: 'line',
      fill: 'tonexty', //fill: 'tozeroy',
      name: 'Ambient [°C]'
    };

    var rel_hum_graph = {
      y: rel_hum_array,
      type: 'line',
      // fill: 'tonexty',
      name: 'RH [%]'
    };

    var bar_pres_graph = {
      y: bar_pres_array,
      type: 'line',
      // fill: 'tonexty',
      name: 'Bar. Press. [KPa]'
    };

    var eco2_graph = {
      y: eco2_array,
      type: 'line',
      // fill: 'tonexty',
      name: 'eCO2 [PPM/10]'
    };

    var etvoc_graph = {
      y: etvoc_array,
      type: 'line',
      fill: 'tonexty',
      name: 'eTVOC [PPB/10]'
    };

    // Order of the curves: first graphs in the array are drawn way in the back
    var data = [spo2_graph, heart_rate_graph, bar_pres_graph, eco2_graph, etvoc_graph, body_temp_graph, amb_temp_graph, rel_hum_graph];

    layout = {
      margin: {
        l: 40,
        r: 80,
        b: 40,
        t: 40,
        pad: 4
      },
      xaxis: {
        title: {
          text: 'Sensor Measurements',
          font: {
            family: 'Courier New, monospace',
            size: 18,
            color: '#7f7f7f'
          }
        },
        // scaleanchor: "y"
      },
      yaxis: {
        // scaleanchor: "x",
        title: {
          //   text: 'Num/%',
          font: {
            family: 'Courier New, monospace',
            size: 18,
            color: '#7f7f7f'
          }
        },
      },
      title: {
        text: '<b>Firefighter Sensor Data</b>',
        font: {
          family: 'Courier New, monospace',
          size: 20
        },
        xref: 'paper',
        x: 0.00,
      },
      plot_bgcolor: "#393d3f", // https://coolors.co/393d3f-fdfdff-c6c5b9-62929e-546a7b
      paper_bgcolor: "#fdfdff"
    };

    var config = { responsive: true }

    Plotly.react('chart', data, layout, config);
  });
}; ////// END: -------- Update sensor graph  ---------------------------

//// ---------------- Update heatmap for the selected sensor ----------------
function updateSensorHeatmap(obj, point, heatmap_index) {
  // console.log("sel_sensor_name = " + sel_sensor_name);
  max_reading_weight = 100;

  var eco2_min = 300;
  var eco2_max = 2500;

  var body_temp_min = 0;
  var body_temp_max = 100;

  var heart_rate_min = 0;
  var heart_rate_max = 185;

  var spo2_min = 65; // %
  var spo2_max = 100; // %

  var amb_temp_min = 0;
  var amb_temp_max = 60;

  var rel_hum_min = 0;
  var rel_hum_max = 100;

  var bar_press_min = 0; //KPa
  var bar_press_max = 114;

  var etvoc_min = 0; //KPa
  var etvoc_max = 114;

  // Compute normalized weights for each sensor reading type for comparative visualization
  // with the heatmap layer
  if (sel_sensor_name == 'body_temp') {
    var body_temp_reading = parseInt(obj.body_temp);
    if (body_temp_reading > body_temp_max) { body_temp_reading = body_temp_max; }
    else if (body_temp_reading < body_temp_min) { body_temp_reading = body_temp_min; }

    var normalized_body_temp_reading = body_temp_reading / (body_temp_max - body_temp_min);
    reading_weight = normalized_body_temp_reading * max_reading_weight;
  }

  if (sel_sensor_name == 'heart_rate') {
    var heart_rate_reading = parseInt(obj.heart_rate);
    if (heart_rate_reading > heart_rate_max) { heart_rate_reading = heart_rate_max; }
    else if (heart_rate_reading < heart_rate_min) { heart_rate_reading = heart_rate_min; }

    var normalized_heart_rate_reading = heart_rate_reading / (heart_rate_max - heart_rate_min);
    reading_weight = normalized_heart_rate_reading * max_reading_weight;
  }

  if (sel_sensor_name == 'spo2') {
    var spo2_reading = parseInt(obj.spo2);
    if (spo2_reading > spo2_max) { spo2_reading = spo2_max; }
    else if (spo2_reading < spo2_min) { spo2_reading = spo2_min; }

    var normalized_spo2_reading = spo2_reading / (spo2_max - spo2_min);
    reading_weight = normalized_spo2_reading * max_reading_weight;
  }

  if (sel_sensor_name == 'amb_temp') {
    var amb_temp_reading = parseInt(obj.amb_temp);
    if (amb_temp_reading > amb_temp_max) { amb_temp_reading = amb_temp_max; }
    else if (amb_temp_reading < amb_temp_min) { amb_temp_reading = amb_temp_min; }

    var normalized_amb_temp_reading = amb_temp_reading / (amb_temp_max - amb_temp_min);
    reading_weight = normalized_amb_temp_reading * max_reading_weight;
  }

  if (sel_sensor_name == 'rel_hum') {
    var rel_hum_reading = parseInt(obj.rel_hum);
    if (rel_hum_reading > rel_hum_max) { rel_hum_reading = rel_hum_max; }
    else if (rel_hum_reading < rel_hum_min) { rel_hum_reading = rel_hum_min; }

    var normalized_rel_hum_reading = rel_hum_reading / (rel_hum_max - rel_hum_min);
    reading_weight = normalized_rel_hum_reading * max_reading_weight;
  }

  if (sel_sensor_name == 'bar_press') {
    var bar_press_reading = parseInt(obj.bar_press);
    if (bar_press_reading > bar_press_max) { bar_press_reading = bar_press_max; }
    else if (bar_press_reading < bar_press_min) { bar_press_reading = bar_press_min; }

    var normalized_bar_press_reading = bar_press_reading / (bar_press_max - bar_press_min);
    reading_weight = normalized_bar_press_reading * max_reading_weight;
  }

  if (sel_sensor_name == 'eco2') {

    var eco2_reading = parseInt(obj.eco2);
    if (eco2_reading > eco2_max) { eco2_reading = eco2_max; }
    else if (eco2_reading < eco2_min) { eco2_reading = eco2_min; }

    var normalized_eco2_reading = eco2_reading / (eco2_max - eco2_min);
    reading_weight = normalized_eco2_reading * max_reading_weight;
  }

  if (sel_sensor_name == 'etvoc') {

    var etvoc_reading = parseInt(obj.etvoc);
    if (etvoc_reading > etvoc_max) { etvoc_reading = etvoc_max; }
    else if (etvoc_reading < etvoc_min) { etvoc_reading = etvoc_min; }

    var normalized_etvoc_reading = etvoc_reading / (etvoc_max - etvoc_min);
    reading_weight = normalized_etvoc_reading * max_reading_weight;
  }

  //// ------ Heatmap layer --------
  // console.log("reading_weight = " + reading_weight);

  heatmap_point = { location: point, weight: reading_weight };
  heatmap_array[heatmap_index] = heatmap_point;
}

function crew_memberChange() {
  var crewMemberDropdown = document.getElementById("crew_member");
  window.sel_crew_member_name = crewMemberDropdown.options[crewMemberDropdown.selectedIndex].text;

  console.log('sel_crew_member_name: ', window.sel_crew_member_name)
}

function sensorDisplayChange() {
  window.sel_sensor_index = document.getElementById("heatmap_select").value;
  //   sensor_name = document.getElementById("heatmap_select").textContent;
  var sensorSelectDropdown = document.getElementById("heatmap_select");
  window.sel_sensor_name = sensorSelectDropdown.options[sensorSelectDropdown.selectedIndex].text;

  //   console.log('sel_sensor_index: ', window.sel_sensor_index)
  console.log('sensor_name: ', window.sel_sensor_name)
}

////// -------- Get current weather ---------------------------
function getCurrentWeather() {
  
  if (window.main_latitude != null && window.main_longitude != null) {
    link = "https://api.openweathermap.org/data/2.5/weather?lat=" + window.main_latitude
      + "&lon=" + window.main_longitude + "&units=metric&apikey=<put-you-api-key-here>";

    var request = new XMLHttpRequest();

    request.open('GET', link, true);

    request.onload = function () {
      var obj = JSON.parse(this.response);
      if (request.status >= 200 && request.status < 400) {
        // var temp = obj.main.temp;
        var temp = obj.main.temp;
        var humidity = obj.main.humidity;
        var weather_main = obj.weather[0].main;
        var weather_description = obj.weather[0].description;
        var wind_speed = obj.wind.speed;
        var wind_direction = obj.wind.deg;
        var cloudiness = obj.clouds.all;


        console.log("CURRENT WEATHER: You have it!", temp);
        document.getElementById('current_weather').innerHTML = "<strong>" + weather_icon_tag + "Current Weather:</strong><br>"
          + 'Temp: ' + temp + "°C | RH: " + humidity
          + "% | Weather: " + weather_main + ", " + weather_description
          + " | Wind: " + wind_speed + "m/s, " + wind_direction + "° | Clouds: " + cloudiness + "%";
      }
      else {
        console.log("CURRENT WEATHER: That didn't work...");
      }
    }
    request.send();
  }
} ////// END: -------- Get current weather ---------------------------

////// -------- Get the weather forecast ---------------------------
function getWeatherForecast() {
  if (window.main_latitude != null && window.main_longitude != null) {
    link = "https://api.openweathermap.org/data/2.5/forecast?lat=" + window.main_latitude
      + "&lon=" + window.main_longitude + "&units=metric&apikey=<put-you-api-key-here>";

    var request = new XMLHttpRequest();

    request.open('GET', link, true);

    request.onload = function () {
      var obj = JSON.parse(this.response);

      var num_forecasts = 8;

      var weather_forecast_html = "<strong>" + weather_forecast_icon_tag + "Three-hour Forecasts:</strong>";
      if (request.status >= 200 && request.status < 400) {
        for (var idx = 0; idx < num_forecasts; ++idx) {
          var temp = obj.list[idx].main.temp;
          var humidity = obj.list[idx].main.humidity;
          var weather_main = obj.list[idx].weather[0].main;
          var weather_description = obj.list[idx].weather[0].description;
          var wind_speed = obj.list[idx].wind.speed;
          var wind_direction = obj.list[idx].wind.deg;
          var cloudiness = obj.list[idx].clouds.all;

          // Sometimes the following element is absent. Defaulting to zero:
          var rain_3hr = "0";
          if (obj.list[idx].rain && obj.list[idx].rain['3h']) {
            var rain_3hr = obj.list[idx].rain['3h'];
          }

          weather_forecast_html = weather_forecast_html + '<br><strong>+' + (idx + 1) * 3 + 'Hrs></strong> Temp: ' + temp + "°C | RH: " + humidity
            + "% | Weather: " + weather_main + ", " + weather_description
            + " | Rain: " + rain_3hr
            + "mm | Wind: " + wind_speed + "m/s, " + wind_direction + "° | Clouds: " + cloudiness + "%";
        }
        console.log("WEATHER FORECAST: You have it!", temp);

        document.getElementById('weather_forecast').innerHTML = weather_forecast_html;

      }
      else {
        console.log("WEATHER FORECAST: That didn't work...");
      }
    }
    request.send();
  }
} ////// END: -------- Get the weather forecast ---------------------------


function doNothing() {/* Some day I will be doing something! */ }


